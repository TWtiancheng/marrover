package rover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DirectionTest {
    Direction northDirection = Direction.N;
    Direction southDirection = Direction.S;
    Direction eastDirection = Direction.E;
    Direction westDirection = Direction.W;

    @Test
    void turnRightTest() {
        assertEquals(eastDirection, northDirection.turnRight());
        assertEquals(southDirection, eastDirection.turnRight());
        assertEquals(westDirection, southDirection.turnRight());
        assertEquals(northDirection, westDirection.turnRight());
    }

    @Test
    void turnLeftTest() {
        assertEquals(westDirection, northDirection.turnLeft());
        assertEquals(northDirection, eastDirection.turnLeft());
        assertEquals(eastDirection, southDirection.turnLeft());
        assertEquals(southDirection, westDirection.turnLeft());
    }
}
