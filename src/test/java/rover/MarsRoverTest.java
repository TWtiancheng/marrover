package rover;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MarsRoverTest {

    private MarsRover rover;

    @BeforeEach
    public void setUp() throws Exception {
        rover = new MarsRover(1, 1, Direction.N);
    }

    @Test
    public void shouldMoveRoverAround() throws Exception {
        assertEquals("1 3 N", rover.run(Arrays.asList(new MoveCommand(), new MoveCommand())));
    }

    @Test
    public void shouldTurnRoverRight() throws Exception {
        assertEquals("1 1 W", rover.run(Arrays.asList(new RightCommand(), new RightCommand(), new RightCommand())));
    }

    @Test
    public void shouldTurnRoverLeft() throws Exception {
        assertEquals("1 1 E", rover.run(Arrays.asList(new LeftCommand(), new LeftCommand(), new LeftCommand())));
    }

    @Test
    public void shouldCircleAround() throws Exception {
        assertEquals("1 1 N", rover.run(Arrays.asList(new MoveCommand(), new RightCommand(),
                new MoveCommand(), new RightCommand(),
                new MoveCommand(), new RightCommand(),
                new MoveCommand(), new RightCommand())));
        assertEquals("1 1 N", rover.run(Arrays.asList(new MoveCommand(), new LeftCommand(),
                new MoveCommand(), new LeftCommand(),
                new MoveCommand(), new LeftCommand(),
                new MoveCommand(), new LeftCommand())));
    }

}
