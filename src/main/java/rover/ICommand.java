package rover;

public interface ICommand {

    void execute(MarsRover marsRover);
}
