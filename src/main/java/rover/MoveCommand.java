package rover;

public class MoveCommand implements ICommand{

    @Override
    public void execute(MarsRover marsRover) {
        marsRover.move();
    }
}
