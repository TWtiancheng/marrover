package rover;

import javafx.util.Pair;

public enum Direction implements Turnable {
    N(new Pair<>(0, 1)) {
        @Override
        public Direction turnRight() {
            return E;
        }

        @Override
        public Direction turnLeft() {
            return W;
        }
    },
    S(new Pair<>(0, -1)) {
        @Override
        public Direction turnRight() {
            return W;
        }

        @Override
        public Direction turnLeft() {
            return E;
        }
    },
    E(new Pair<>(1, 0)) {
        @Override
        public Direction turnRight() {
            return S;
        }

        @Override
        public Direction turnLeft() {
            return N;
        }
    },
    W(new Pair<>(-1, 0)) {
        @Override
        public Direction turnRight() {
            return N;
        }

        @Override
        public Direction turnLeft() {
            return S;
        }
    };

    private Pair<Integer, Integer> moveXY;

    Direction(Pair<Integer, Integer> moveXY) {
        this.moveXY = moveXY;
    }

    public Pair<Integer, Integer> getMoveXY() {
        return moveXY;
    }
}
