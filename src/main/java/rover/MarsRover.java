package rover;

import java.util.Arrays;
import java.util.List;

public class MarsRover {
    private static final List<String> VALID_COMMANDS = Arrays.asList("L", "R", "M");

    private Direction direction;

    private Coordinate coordinate;

    public MarsRover(int startingX, int startingY, Direction direction) {
        this.coordinate = new Coordinate(startingX, startingY);
        this.direction = direction;
    }

    public String run(List<ICommand> commands) {


        for (ICommand command : commands) {
            command.execute(this);
        }

        return asString();
    }

    public void move() {
        coordinate = coordinate.update(direction);
    }

    private String asString() {
        return coordinate.x + " " + coordinate.y + " " + direction;
    }

    public void turnLeft() {
        direction = direction.turnLeft();
    }

    public void turnRight() {
        direction = direction.turnRight();
    }

    private static String[] convertInputIntoCommands(String input) {
        String[] commandArray = input.split("(?!^)");

        validateCommands(input, commandArray);

        return commandArray;
    }

    private static void validateCommands(String input, String[] commandArray) {
        for (String command : commandArray) {
            if (!VALID_COMMANDS.contains(command)) {
                throw new IllegalArgumentException("Invalid command sequence: " + input);
            }
        }
    }


}
