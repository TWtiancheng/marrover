package rover;

public class RightCommand implements ICommand{

    @Override
    public void execute(MarsRover marsRover) {
        marsRover.turnRight();
    }
}
