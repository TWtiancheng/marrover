package rover;

public class Coordinate {
    int x;
    int y;

    Coordinate(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Coordinate update(Direction direction){
        return new Coordinate(x + direction.getMoveXY().getKey(), y + direction.getMoveXY().getValue());
    }

}
