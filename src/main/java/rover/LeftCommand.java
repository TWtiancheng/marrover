package rover;

public class LeftCommand implements ICommand{

    @Override
    public void execute(MarsRover marsRover) {
        marsRover.turnLeft();
    }
}
